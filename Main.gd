extends Spatial
var VR: MobileVRInterface

func _ready():
	print(str(ARVRServer.get_interfaces()))
	VR = ARVRServer.find_interface("Native mobile")
	if VR and VR.initialize():
		get_viewport().arvr = true
		get_viewport().hdr = true
		VR.display_to_lens = 2.0
		OS.vsync_enabled = false
		Engine.target_fps = 90
		# Also, the physics FPS in the project settings is also 90 FPS. This makes the physics
		# run at the same frame rate as the display, which makes things look smoother in VR!

func _process(delta):
	pass
	#get_viewport().get_camera().global_transform.basis = get_viewport().get_camera().global_transform.basis.orthonormalized()
